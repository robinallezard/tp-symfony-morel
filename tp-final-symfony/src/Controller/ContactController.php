<?php

namespace App\Controller;
use App\Entity\User;
use App\Form\UserType;
use AppBundle\Form\ProductType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\FormType; 
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Validator\Constraints as Assert;


class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function showUser() 
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();

       return $this->render('contact/all.html.twig', [
           'users' => $users
       ]);
    }

    /**
     * @Route("/contact/new", name="contact_new")
     */
    public function newUser(Request $request)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('civilite', ChoiceType::class, [
                'choices'  => [
                    'Monsieur' => '0',
                    'Madame' => '1',
                    'mademoiselle' => '-1']])
            ->add('name', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('firstname', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('email', EmailType::class, ['attr' => ['class' => 'form-control']])
            ->add('phone', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('newsletter', CheckboxType::class, ['attr' => ['class' => 'form-control']])
            ->add('save', SubmitType::class, ['attr' => ['class' => 'save']])
            ->getForm();

        $form->handleRequest($request);

        $id = $user->getId();

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('contact'));

            $this->addFlash('message', "l'article ".$id." a été ajouté");

            return $this->redirectToRoute('article_all');
        }

        return $this->render('contact/new.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }
}
